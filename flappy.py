"""Flappy, game inspired by Flappy Bird.

Exercises

1. Keep score.
2. Vary the speed.
3. Vary the size of the balls.
4. Allow the bird to move forward and back.

"""

from random import *
from turtle import *
from freegames import vector

state = {'score': 0}
writer = Turtle(visible=False)
bird = vector(0, 0)
balls = []


class Ball:
    def __init__(self, speed, size, y):
        self.vector = vector(199, y)
        self.speed = speed
        self.size = size


def keyboard_move(x, y):
    "Move bird up in response to keyboard arrows tap."
    key_move = vector(x, y)
    bird.move(key_move)


def tap(x, y):
    "Move bird up in response to screen tap."
    up = vector(0, 30)
    bird.move(up)


def inside(point):
    "Return True if point on screen."
    return -200 < point.x < 200 and -200 < point.y < 200


def draw(alive):
    "Draw screen objects."
    clear()

    goto(bird.x, bird.y)

    if alive:
        state['score'] += 1  # get points while still alive
        dot(10, 'green')
    else:
        for ball in balls:  # when game is over, hide all balls
            ball.size = 0
        dot(10, 'red')

    for ball in balls:
        goto(ball.vector.x, ball.vector.y)
        dot(ball.size, 'black')  # each ball get his own size
    # update()


def move():
    "Update object positions."
    # write high score to screen
    writer.undo()
    writer.write("Score: {}".format(state['score']), False, align="left", font={"Arial", 8, "normal"})

    bird.y -= 5

    for ball in balls:
        ball.vector.x -= ball.speed  # each ball moves with his own speed

    if randrange(10) == 0:
        y = randrange(-199, 199)
        size = randint(10, 50)
        speed = randrange(1, 10)
        ball = Ball(speed, size, y)  # instantiate each ball with his own attributes
        balls.append(ball)

    while len(balls) > 0 and not inside(balls[0].vector):
        balls.pop(0)

    if not inside(bird):
        draw(False)
        return

    for ball in balls:
        if abs(ball.vector - bird) < 15:
            draw(False)
            return

    draw(True)
    ontimer(move, 50)


setup(420, 420, 370, 0)
hideturtle()
up()
tracer(False)
onscreenclick(tap)

writer.goto(-30, 180)
writer.color('black')
writer.write(state['score'])
# listen to keyboard
listen()
onkey(lambda: keyboard_move(0, 30), "Up")
onkey(lambda: keyboard_move(-30, 0), "Left")
onkey(lambda: keyboard_move(30, 0), "Right")

move()
done()
